﻿using System;

namespace Assets.Scripts
{
    [Serializable]
    public enum ExportFileFormat
    {
        Fbx,
        Obj,
    }
}