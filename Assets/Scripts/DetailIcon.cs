﻿

using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class DetailIcon : MonoBehaviour
    {
        public Text Text;
        public Image Icon;

        public void SetIcon(string detailName)
        {
            var icon = AddDetailPanel.GetDetailIcon(detailName);
            var hasIcon = icon != null;

            Text.gameObject.SetActive(!hasIcon);
            Icon.gameObject.SetActive(hasIcon);

            if (hasIcon) {
                Icon.sprite = icon;
            } else {
                Text.text = detailName;
            }
        }
    }
}
