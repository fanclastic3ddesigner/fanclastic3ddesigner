﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Assets.Scripts {

    public class Session
    {
        public SceneData SceneData { get; private set; }

        public string FileName => Path.GetFileNameWithoutExtension(_filePath);

        private readonly Dictionary<int, Detail> _id2Detail = new Dictionary<int, Detail>();
        private List<InstructionBase> _sourceInstructions;
        private string _filePath;
        private AdditionalLoadingData _additionalLoadingData;

        public Detail GetDetail(int id)
        {
            return _id2Detail[id];
        }

        public Session()
        {
            AppController.Instance.ActionsLog.Clear();
            RemoveAll();
            _sourceInstructions = new List<InstructionBase>();
            SceneData = null;
        }

        public Session(FileInfo fileInfo)
        {
            AppController.Instance.ActionsLog.Clear();
            LoadFile(fileInfo, false);
        }

        public Session(string modelName)
        {
            AppController.Instance.ActionsLog.Clear();
            LoadModelsSetsFile(modelName, false);
        }

        public void LoadModelAdditive(string modelName)
        {
            LoadModelsSetsFile(modelName, true);
        }

        public void LoadFileAdditive(FileInfo fileInfo)
        {
            LoadFile(fileInfo, true);
        }

        public void Reset()
        {
            //TODO тут сделать сброс деталей в последнее сохраненное состояние вместо повторной загрузки файла
            AppController.Instance.ActionsLog.Clear();
            LoadFromData(SceneData);
        }

        public void Save(string fileName)  //TODO добавить заголовок, чтобы определять свои файлы
        {
            var roots = new List<GameObject>();

            _id2Detail.Clear();
            UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects(roots);

            SceneData = new SceneData();

            var mergedInstructions = GetMergedInstructions();

            foreach (var root in roots) {
                var detailBase = root.GetComponent<DetailBase>();

                if (detailBase == null) {
                    continue;
                }

                if (!detailBase.gameObject.activeSelf) {
                    continue;
                }

                var detail = detailBase as Detail;

                if (detail != null) {
                    SceneData.SingleDetails.Add(detail.Data);
                    _id2Detail.Add(detail.GetInstanceID(), detail);
                    continue;
                }

                var detailsGroup = (DetailsGroup) detailBase;
                var connectedGroup = new ConnectedGroup();

                foreach (var child in detailsGroup.Details) {
                    connectedGroup.Details.Add(child.Data);
                    _id2Detail.Add(child.GetInstanceID(), child);
                }

                SceneData.ConnectedGroups.Add(connectedGroup);
            }


            var group2Instructions = SplitInstructions(_id2Detail, mergedInstructions);

            foreach (var connectedGroup in SceneData.ConnectedGroups)
            {
                var id = connectedGroup.Details.First().Id;
                var group = _id2Detail[id].Group;

                connectedGroup.Instructions = group2Instructions[group];

                for (var i = 0; i < connectedGroup.Instructions.Count; i++) {
                    connectedGroup.Instructions[i].Step = i + 1;
                }
            }

            var checkedFileName = fileName.EndsWith(".fcl") ? fileName : (fileName + ".fcl");
            var settings = new JsonSerializerSettings {
                TypeNameHandling = TypeNameHandling.Auto
            };

            var dataJson = JsonConvert.SerializeObject(SceneData, Formatting.Indented, settings);

            File.WriteAllText(checkedFileName, dataJson);
            _filePath = checkedFileName;

            Debug.Log("Saved: " + checkedFileName);
        }

        private List<InstructionBase> GetMergedInstructions()
        {
            HashSet<Detail> invalidDetails;
            var actionsLog = AppController.Instance.ActionsLog;
            var newInstructions = actionsLog.GetInstructions(out invalidDetails);
            var mergedInstructions = new List<InstructionBase>();
            var invalidIds = new HashSet<int>(invalidDetails.Select(detail => detail.GetInstanceID()));

            foreach (var instruction in _sourceInstructions)
            {
                var mergedTargetDetails = new HashSet<int>(instruction.TargetDetails);

                mergedTargetDetails.ExceptWith(invalidIds);

                var mergedInstruction = instruction.Copy(mergedTargetDetails);

                if (mergedInstruction.TargetDetails.Any()) {
                    mergedInstructions.Add(mergedInstruction);
                }
            }

            mergedInstructions.AddRange(newInstructions);
            return mergedInstructions;
        }

        private Dictionary<DetailsGroup, List<InstructionBase>> SplitInstructions(Dictionary<int, Detail> id2Detail, List<InstructionBase> mergedInstrucions)
        {
            var group2Instructions = new Dictionary<DetailsGroup, List<InstructionBase>>();

            foreach (var instruction in mergedInstrucions)
            {
                if (instruction.TargetDetails.Count == 1)
                {
                    var id = instruction.TargetDetails.First();
                    var group = id2Detail[id].Group;

                    if (group == null) {
                        continue;
                    }

                    if (!group2Instructions.ContainsKey(group)) {
                        group2Instructions.Add(group, new List<InstructionBase>());
                    }
                    group2Instructions[group].Add(instruction);
                    continue;
                }

                var group2TargetDetails = instruction.TargetDetails.ToLookup(id => id2Detail[id].Group);

                foreach (var targetDetail in group2TargetDetails)
                {
                    var group = targetDetail.Key;
                    var targetDetails = new HashSet<int>(group2TargetDetails[group]);

                    if (group == null) {
                        continue;
                    }

                    if (!group2Instructions.ContainsKey(group)) {
                        group2Instructions.Add(group, new List<InstructionBase>());
                    }
                    var instructionCopy = instruction.Copy(targetDetails);
                    group2Instructions[group].Add(instructionCopy);
                }
            }

            return group2Instructions;
        }

        private void LoadFile(FileSystemInfo fileInfo, bool additive)
        {
            var filePath = fileInfo.FullName;
            var fileData = FileDataLoader.LoadData(fileInfo);

            if (additive)
            {
                LoadFromDataAdditive(fileData);
            }
            else
            {
                _filePath = filePath;
                LoadFromData(fileData);
            }

            Debug.Log("Loaded: " + filePath + (additive ? " additive" : string.Empty));
        }

        private void LoadModelsSetsFile(string modelName, bool additive)
        {
            var sceneData = FileDataLoader.LoadDataFromSets(modelName);

            if (additive)
            {
                LoadFromDataAdditive(sceneData);
            }
            else
            {
                _filePath = "";
                LoadFromData(sceneData);
            }
            
            Debug.Log("Loaded model from sets: " + modelName + (additive ? " additive" : string.Empty));
        }

        private void LoadFromData(SceneData sceneData) {

            RemoveAll();

            SceneData = sceneData;

            CreateObjects(SceneData, out var instructions, out _);
            _sourceInstructions = instructions;
        }

        private void LoadFromDataAdditive(SceneData sceneData)
        {
            var selected = AppController.Instance.SelectedDetails;

            CreateObjects(sceneData, out var newInstructions, out var newRootDetails);

            var allRootDetails = UnityEngine.SceneManagement.SceneManager.GetActiveScene()
                .GetRootGameObjects()
                .Select(obj => obj.GetComponent<DetailBase>())
                .Where(obj => obj != null && obj.gameObject.activeSelf)
                .ToHashSet();
            var currentRootDetails = allRootDetails
                .Where(rootDetail => !newRootDetails.Contains(rootDetail))
                .ToHashSet();

            _additionalLoadingData = new AdditionalLoadingData
            {
                CurrentRootDetails = currentRootDetails,
                NewRootDetails = newRootDetails,
                SelectedDetail = selected.Selected,
                NewInstructions = newInstructions
            };
            
            ShowAdditiveLoadingPositionSelection(_additionalLoadingData);
        }

        private void ShowAdditiveLoadingPositionSelection(AdditionalLoadingData additionalLoadingData)
        {
            var additiveLoaderPositionSelectionLayer = AppController.Instance.AdditiveLoaderPositionSelectionLayer;
            var currentInstructions = GetMergedInstructions();
            var currentSingleDetails = additionalLoadingData.CurrentRootDetails.OfType<Detail>().ToHashSet();
            var newSingleDetails = additionalLoadingData.NewRootDetails.OfType<Detail>().ToHashSet();
            var usedArea = GetFloorProjectionArea(currentSingleDetails, currentInstructions);
            var newArea = GetFloorProjectionArea(newSingleDetails, additionalLoadingData.NewInstructions);
            
            SetAdditionalLoadingModeEnabled(true);
            additiveLoaderPositionSelectionLayer.StartSelection(usedArea, newArea, OnAdditiveLoadingPositionConfirmed, OnAdditiveLoadingCanceled);
        }

        private void SetAdditionalLoadingModeEnabled(bool isEnabled, bool destroyNewDetails = false)
        {
            var appController = AppController.Instance;

            if (isEnabled)
                appController.SelectedDetails.Clear();
            else
                appController.SelectedDetails.Add(_additionalLoadingData.SelectedDetail);

            foreach (var rootDetail in _additionalLoadingData.CurrentRootDetails)
            {
                rootDetail.gameObject.SetActive(!isEnabled);
            }
            foreach (var rootDetail in _additionalLoadingData.NewRootDetails)
            {
                if (destroyNewDetails)
                    Object.Destroy(rootDetail);
                else 
                    rootDetail.gameObject.SetActive(!isEnabled);
            }
            appController.ModeSwitcher.gameObject.SetActive(!isEnabled);
            appController.EditorLayer.gameObject.SetActive(!isEnabled);
        }
        
        private void OnAdditiveLoadingPositionConfirmed(Vector2Int offset)
        {
            if (offset != Vector2Int.zero)
            {
                var offsetVector3 = new SerializableVector3(offset.x, 0, offset.y);
                
                foreach (var rootDetail in _additionalLoadingData.NewRootDetails)
                {
                    rootDetail.transform.position += (Vector3) offsetVector3;
                }
                foreach (var newInstructionBase in _additionalLoadingData.NewInstructions)
                {
                    switch (newInstructionBase)
                    {
                        case InstructionAdd instructionAdd:
                            instructionAdd.Position += offsetVector3;
                            break;
                        case InstructionMoveAndRotate instructionMoveAndRotate:
                            instructionMoveAndRotate.Pivot += offsetVector3;
                            break;
                        default: continue;
                    }
                }
            }

            var newDetailsSet = new HashSet<Detail>();

            foreach (var baseDetail in _additionalLoadingData.NewRootDetails)
            {
                switch (baseDetail)
                {
                    case Detail detail:
                        newDetailsSet.Add(detail);
                        break;
                    case DetailsGroup detailsGroup:
                        newDetailsSet.UnionWith(detailsGroup.Details);
                        break;
                }
            }
            
            SetAdditionalLoadingModeEnabled(false);
            AppController.Instance.ActionsLog.RegisterAction(
                new LoadAdditiveAction(newDetailsSet, _additionalLoadingData.NewInstructions));
        }

        private void OnAdditiveLoadingCanceled()
        {
            SetAdditionalLoadingModeEnabled(false, true);
        }
        
        private HashSet<Vector2Int> GetFloorProjectionArea(HashSet<Detail> singleDetails, List<InstructionBase> instructions)
        {
            var result = new HashSet<Vector2Int>();

            foreach (var singleDetail in singleDetails)
            {
                result.UnionWith(GetDetailFloorProjectionArea(singleDetail));
            }
            
            foreach (var instruction in instructions)
            {
                if (!instruction.TargetDetails.Any())
                    continue;
                instruction.Do();
                foreach (var targetDetailId in instruction.TargetDetails)
                {
                    var targetDetail = _id2Detail[targetDetailId];
                    result.UnionWith(GetDetailFloorProjectionArea(targetDetail));
                }
            }

            return result;
        }

        private HashSet<Vector2Int> GetDetailFloorProjectionArea(Detail detail)
        {
            var result = new HashSet<Vector2Int>();
            var detailBounds = detail.Bounds;
            var projectionSizeX =  Mathf.RoundToInt(detailBounds.size.x);
            var projectionSizeZ = Mathf.RoundToInt(detailBounds.size.z);
            var projectionMinX = Mathf.RoundToInt(detailBounds.min.x);
            var projectionMinZ = Mathf.RoundToInt(detailBounds.min.z);

            for (var x = 0; x < projectionSizeX; x++)
            {
                for (var z = 0; z < projectionSizeZ; z++)
                {
                    result.Add(new Vector2Int(projectionMinX + x, projectionMinZ + z));
                }
            }

            return result;
        }
        
        private void RemoveAll() {
            var rootObjects = new List<GameObject>();
            UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects(rootObjects);

            foreach (var rootObject in rootObjects) {
                var detailBase = rootObject.GetComponent<DetailBase>();

                if (detailBase != null) {
                    Object.DestroyImmediate(rootObject);
                }
            }

            _id2Detail.Clear();
            _sourceInstructions = null;
            SceneData = null;
        }

        private void CreateObjects(SceneData sceneData, out List<InstructionBase> instructions, out HashSet<DetailBase> rootDetails)
        {
            instructions = new List<InstructionBase>();
            rootDetails = new HashSet<DetailBase>();
            
            var oldId2Detail = new Dictionary<int, Detail>();
            var links2Connections = new Dictionary<LinksBase, List<int>>();

            foreach (var detailData in sceneData.SingleDetails) {
                rootDetails.Add(CreateDetail(detailData));
            }

            foreach (var connectedGroup in sceneData.ConnectedGroups) {
                var detailsGroup = DetailsGroup.CreateNewGroup(Vector3.zero, Quaternion.identity);

                foreach (var detailData in connectedGroup.Details) {
                    var oldId = detailData.Id;
                    var newDetail = CreateDetail(detailData);

                    links2Connections.Add(newDetail.Links, detailData.Connections);
                    oldId2Detail.Add(oldId, newDetail);

                    detailsGroup.Add(newDetail);
                }
                rootDetails.Add(detailsGroup);
                instructions.AddRange(connectedGroup.Instructions);
            }

            foreach (var detailLinks in links2Connections.Keys) {
                var connections = links2Connections[detailLinks];

                foreach (var id in connections) {
                    detailLinks.Connections.Add(oldId2Detail[id]);
                }
                connections.Clear();
                connections.AddRange(detailLinks.Connections.Select(detail => detail.Data.Id));
            }

            foreach (var instruction in instructions)
            {
                var newTargetDetails = new HashSet<int>();

                foreach (var detailId in instruction.TargetDetails) {
                    newTargetDetails.Add(oldId2Detail[detailId].Data.Id);
                }

                instruction.TargetDetails = newTargetDetails;
            }
        }

        private Detail CreateDetail(DetailData detailData) {
            var newDetail = AddDetailPanel.GetDetail(detailData.Type);
            var newId = newDetail.GetInstanceID();

            _id2Detail.Add(newId, newDetail);
            detailData.Id = newId;
            detailData.Detail = newDetail;

            ResetDetail(detailData);
            
            return newDetail;
        }

        private void ResetDetail(DetailData detailData)
        {
            var detail = detailData.Detail;
            var transform = detail.transform;

            transform.position = detailData.Position;
            transform.eulerAngles = detailData.Rotation;
            if (!detail.FixedColor) {
                detail.Color = AddDetailPanel.GetColor(detailData.Color);
            }
            
            detail.gameObject.SetActive(true);
        }
    }
}
