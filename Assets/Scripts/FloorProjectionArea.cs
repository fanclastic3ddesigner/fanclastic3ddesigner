﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    public class FloorProjectionArea : MonoBehaviour
    {
        [SerializeField]
        private GameObject _cellPrefab;
        
        private readonly List<SpriteRenderer> _cellsPool = new();
        private readonly Dictionary<Vector2Int, SpriteRenderer> _areaPositionToCellMap = new();

        public virtual HashSet<Vector2Int> AreaPositions { get; private set; }
        
        public virtual void SetArea(HashSet<Vector2Int> areaPositions)
        {
            AreaPositions = areaPositions;
            ReturnCellsToPool(_areaPositionToCellMap.Values);
            _areaPositionToCellMap.Clear();

            foreach (var position in AreaPositions)
            {
                var cell = GetCellFromPool();
                var cellTransform = cell.transform;

                cellTransform.position = new Vector3(position.x, 0, position.y);
                _areaPositionToCellMap.Add(position, cell);
            }
        }

        public virtual void SetPositionColor(Vector2Int position, Color color)
        {
            _areaPositionToCellMap[position].color = color;
        }
        
        private SpriteRenderer GetCellFromPool()
        {
            if (_cellsPool.Count == 0)
            {
                for (var i = 0; i < 10; i++)
                {
                    _cellsPool.Add(Instantiate(_cellPrefab, transform).GetComponent<SpriteRenderer>());
                }
            }

            var lastIndex = _cellsPool.Count - 1;
            var cell = _cellsPool[lastIndex];
            
            cell.gameObject.SetActive(true);
            _cellsPool.RemoveAt(lastIndex);

            return cell;
        }

        private void ReturnCellsToPool(IEnumerable<SpriteRenderer> cells)
        {
            foreach (var cell in cells)
            {
                ReturnCellToPool(cell);
            }
        }
        
        private void ReturnCellToPool(SpriteRenderer cell)
        {
            _cellsPool.Add(cell);
            cell.gameObject.SetActive(false);
        }
    }
}