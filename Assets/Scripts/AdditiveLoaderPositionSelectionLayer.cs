﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class AdditiveLoaderPositionSelectionLayer : MonoBehaviour
    {
        [SerializeField]
        private GameObject _selectionAreas;
        [SerializeField]
        private FloorProjectionArea _usedSpaceProjectionArea;
        [SerializeField]
        private DraggableFloorProjectionArea _newSpaceProjectionArea;

        [SerializeField]
        private Color _noOverlapsCellColor;
        [SerializeField]
        private Color _overlapedValidCellColor;
        [SerializeField]
        private Color _overlapedInvalidCellColor;

        [SerializeField]
        private Button _confirmationButton;

        private Dictionary<Vector2Int, SpriteRenderer> _usedAreaPositionToCellMap;
        private Dictionary<Vector2Int, SpriteRenderer> _newAreaPositionToCellMap;

        private Action<Vector2Int> _onSelectionConfirmed;
        private Action _onSelectionCanceled;
        
        private bool _isValidPosition;
        
        private void Awake()
        {
            _newSpaceProjectionArea.OnPositionChanged += OnNewSpaceProjectionAreaPositionChanged;
        }

        public void StartSelection(
            HashSet<Vector2Int> usedAreaPositions,
            HashSet<Vector2Int> newAreaPositions,
            Action<Vector2Int> onSelectionConfirmed,
            Action onSelectionCanceled)
        {
            _usedSpaceProjectionArea.SetArea(usedAreaPositions);
            _newSpaceProjectionArea.SetArea(newAreaPositions);

            _onSelectionConfirmed = onSelectionConfirmed;
            _onSelectionCanceled = onSelectionCanceled;
            
            OnNewSpaceProjectionAreaPositionChanged();
            SetActive(true);
        }

        private void OnNewSpaceProjectionAreaPositionChanged()
        {
            var usedAreaPositions = _usedSpaceProjectionArea.AreaPositions;
            var newAreaPositions = _newSpaceProjectionArea.AreaPositions;

            var overlappedPositions = new HashSet<Vector2Int>(usedAreaPositions);
            overlappedPositions.IntersectWith(newAreaPositions);

            _isValidPosition = !overlappedPositions.Any();
            _confirmationButton.interactable = _isValidPosition;
            
            if (_isValidPosition)
            {
                _newSpaceProjectionArea.SetPositionsColor(newAreaPositions, _noOverlapsCellColor);
                return;
            }
            
            var validPositions = new HashSet<Vector2Int>(newAreaPositions);
            validPositions.ExceptWith(overlappedPositions);
            
            _newSpaceProjectionArea.SetPositionsColor(overlappedPositions, _overlapedInvalidCellColor);
            _newSpaceProjectionArea.SetPositionsColor(validPositions, _overlapedValidCellColor);
        }

        public void ConfirmSelection()
        {
            _onSelectionConfirmed.Invoke(_newSpaceProjectionArea.Offset);
            SetActive(false);
        }
        
        public void CancelSelection()
        {
            _onSelectionCanceled.Invoke();
            SetActive(false);
        }

        private void SetActive(bool isActive)
        {
            _selectionAreas.SetActive(isActive);
            gameObject.SetActive(isActive);
        }
        
        private void OnDestroy()
        {
            _newSpaceProjectionArea.OnPositionChanged -= OnNewSpaceProjectionAreaPositionChanged;
        }
    }
}