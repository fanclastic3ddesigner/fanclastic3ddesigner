﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.Scripts
{
    public class DraggableFloorProjectionArea : FloorProjectionArea, IBeginDragHandler, IDragHandler
    {
        [SerializeField]
        private Transform _transform;
        [SerializeField]
        private Camera _camera;
        private Vector3 _beginDragPointerFloorPosition;
        private SerializableVector3Int _beginDragAreaPosition;

        public event Action OnPositionChanged;
        
        public Vector2Int Offset => new (Mathf.RoundToInt(_transform.position.x), Mathf.RoundToInt(_transform.position.z));
        
        public override HashSet<Vector2Int> AreaPositions => base.AreaPositions
            .Select(pos => pos + Offset)
            .ToHashSet();

        public override void SetArea(HashSet<Vector2Int> areaPositions)
        {
            _transform.position = Vector3.zero;
            base.SetArea(areaPositions);
        }

        public void OnDrag(PointerEventData eventData)
        {
            var pointerPosition = eventData.position;
            var currentFloorPosition = PointerPositionToFloorPosition(pointerPosition);
            var delta = currentFloorPosition - _beginDragPointerFloorPosition;
            var alignedDelta = (SerializableVector3Int) delta.AlignAsCross();
            var newPosition = _beginDragAreaPosition + alignedDelta;

            if ((SerializableVector3Int) _transform.position == newPosition)
                return;
            
            _transform.position = newPosition;
            OnPositionChanged?.Invoke();
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            var pointerPosition = eventData.position;
            _beginDragPointerFloorPosition = PointerPositionToFloorPosition(pointerPosition);
            _beginDragAreaPosition = _transform.position.AlignAsCross();
        }

        public void SetPositionsColor(HashSet<Vector2Int> positions, Color color)
        {
            foreach (var position in positions)
            {
                
                SetPositionColor(position, color);
            }
        }

        public override void SetPositionColor(Vector2Int position, Color color)
        {
            base.SetPositionColor(position - Offset, color);
        }

        private Vector3 PointerPositionToFloorPosition(Vector2 pointerPosition)
        {
            var pointerRay = _camera.ScreenPointToRay(pointerPosition);
            var distance = (_transform.position.y - pointerRay.origin.y) / pointerRay.direction.y;
            
            return pointerRay.origin + pointerRay.direction * distance;
        }
    }
}