﻿using System.Collections.Generic;

namespace Assets.Scripts
{
    public struct AdditionalLoadingData
    {
        public HashSet<DetailBase> CurrentRootDetails;
        public HashSet<DetailBase> NewRootDetails;
        public HashSet<Detail> SelectedDetail;
        public List<InstructionBase> NewInstructions;
    }
}