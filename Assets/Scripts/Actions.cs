﻿

using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts {

    public enum ActionType
    {
        Selection,
        Movement,
        Rotation,
        Creation,
        Deleting,
        Coloring,
        AdditiveLoading
    }

    public abstract class ActionBase
    {
        public abstract ActionType Type { get; }

        public abstract void Do();
        public abstract void Undo();
    }

    //TODO make InitialAction base class for SelectAction and CreateAction

    public class SelectAction : ActionBase {
        public override ActionType Type {
            get { return ActionType.Selection; }
        }

        public DetailData SourceState { get; private set; }
        public List<Detail> SelectedDetails { get { return _nextSelection.ToList(); } }

        private readonly HashSet<Detail> _prevSelection;
        private readonly HashSet<Detail> _nextSelection;

        public SelectAction(HashSet<Detail> prevSelection, HashSet<Detail> nextSelection) {
            _prevSelection = prevSelection;
            _nextSelection = nextSelection;
            SourceState = nextSelection.Count == 1 ? nextSelection.First().Data : null;
        }

        public override void Do() {
            var selected = AppController.Instance.SelectedDetails;

            selected.Clear();
            selected.Add(_nextSelection);
        }

        public override void Undo() {
            var selected = AppController.Instance.SelectedDetails;

            selected.Clear();
            selected.Add(_prevSelection);
        }
    }

    public class MoveAction : ActionBase {
        public override ActionType Type {
            get { return ActionType.Movement; }
        }

        public Vector3 Offset { get; private set; }

        public MoveAction(Vector3 offset) {
            Offset = offset;
        }

        public override void Do() {
            var selected = AppController.Instance.SelectedDetails;
            var targetDetail = selected.Detach();

            selected.Move(Offset);
            targetDetail.UpdateLinks();
        }

        public override void Undo() {
            var selected = AppController.Instance.SelectedDetails;
            var targetDetail = selected.Detach();

            selected.Move(-Offset);
            targetDetail.UpdateLinks();
        }
    }

    public class RotateAction : ActionBase {
        public override ActionType Type {
            get { return ActionType.Rotation; }
        }

        public Quaternion RotationDelta { get; private set; }
        public Vector3 Pivot { get; private set; }
        public Vector3 Alignment { get; private set; }

        public RotateAction(Quaternion rotationDelta, Vector3 pivot, Vector3 alignment)
        {
            RotationDelta = rotationDelta;
            Pivot = pivot;
            Alignment = alignment;
        }

        public override void Do()
        {
            var selected = AppController.Instance.SelectedDetails;
            var targetDetail = selected.Detach();

            targetDetail.transform.RotateAndTranslate(Pivot, RotationDelta, Alignment);
            targetDetail.UpdateLinks();
        }

        public override void Undo()
        {
            var selected = AppController.Instance.SelectedDetails;
            var targetDetail = selected.Detach();

            targetDetail.transform.Translate(- Alignment, Space.World);
            targetDetail.transform.Rotate(Pivot, Quaternion.Inverse(RotationDelta));
            targetDetail.UpdateLinks();
        }
    }

    public class CreateAction : ActionBase {
        public override ActionType Type {
            get { return ActionType.Creation; }
        }

        public Detail Detail { get; }

        public DetailData SourceState { get; private set; }

        private readonly HashSet<Detail> _prevSelected;

        public CreateAction(Detail detail, HashSet<Detail> prevSelected) {
            Detail = detail;
            _prevSelected = prevSelected;
            SourceState = detail.Data;
        }

        public override void Do() {
            var selected = AppController.Instance.SelectedDetails;

            selected.Clear();
            selected.Add(Detail);
            selected.Show();
        }

        public override void Undo() {
            var selected = AppController.Instance.SelectedDetails;

            selected.Hide();
            selected.Clear();
            selected.Add(_prevSelected);
        }
    }

    public class DeleteAction : ActionBase {
        public override ActionType Type {
            get { return ActionType.Deleting; }
        }

        private readonly DetailBase _detail;

        public DeleteAction(DetailBase detail) {
            _detail = detail;
        }

        public override void Do() {
            var selected = AppController.Instance.SelectedDetails;

            selected.Hide();
            selected.Clear();
        }

        public override void Undo() {
            var selected = AppController.Instance.SelectedDetails;

            selected.Clear();
            selected.Add(_detail);
            selected.Show();
        }
    }

    public class SetColorAction : ActionBase {
        public override ActionType Type {
            get { return ActionType.Coloring; }
        }

        private readonly Dictionary<Detail, DetailColor> _prevColors = new Dictionary<Detail, DetailColor>();
        private readonly DetailColor _newColor;

        public SetColorAction(HashSet<Detail> targetDetails, DetailColor newColor)
        {
            _newColor = newColor;
            foreach (var detail in targetDetails) {
                _prevColors.Add(detail, detail.Color);
            }
        }

        public override void Do() {
            foreach (var detail in _prevColors.Keys) {
                detail.Color = _newColor;
            }
        }

        public override void Undo() {
            foreach (var prevColor in _prevColors) {
                prevColor.Key.Color = prevColor.Value;
            }
        }
    }

    public class LoadAdditiveAction : ActionBase {
        public override ActionType Type => ActionType.AdditiveLoading;
        public DetailData SourceState { get; }
        public List<Detail> LoadedDetails => _loadedDetails.ToList();

        public List<InstructionBase> Instructions { get; }

        private readonly HashSet<Detail> _loadedDetails;

        public LoadAdditiveAction(HashSet<Detail> loadedDetails, List<InstructionBase> instructions) {
            _loadedDetails = loadedDetails;
            Instructions = instructions;
            SourceState = loadedDetails.Count == 1 ? loadedDetails.First().Data : null;
        }

        public override void Do()
        {
            foreach (var detail in _loadedDetails)
            {
                detail.gameObject.SetActive(true);
            }
        }

        public override void Undo() 
        {
            foreach (var detail in _loadedDetails)
            {
                detail.gameObject.SetActive(false);
            }
        }
    }
}
