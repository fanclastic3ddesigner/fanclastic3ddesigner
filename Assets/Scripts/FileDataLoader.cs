﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using UnityEngine;

namespace Assets.Scripts
{
    public static class FileDataLoader
    {
        public static SceneData LoadData(FileSystemInfo fileInfo)
        {
            if (!fileInfo.Exists)
            {
                throw new FileNotFoundException("File doesn't exist!");
            }

            var filePath = fileInfo.FullName;
            var dataJson = File.ReadAllText(filePath);

            return MigrateDataToActualVersion(dataJson);
        }

        public static SceneData LoadDataFromSets(string modelName)
        {
            var modelsPath = AppController.Instance.ModelsSetsLayer.ModelsFilesPath;
            var filePath = Path.Combine(modelsPath, modelName);
            var dataJson = Resources.Load<TextAsset>(filePath).text;

            return MigrateDataToActualVersion(dataJson);
        }
        
        private static SceneData MigrateDataToActualVersion(string dataJson)
        {
            var settings = new JsonSerializerSettings {
                TypeNameHandling = TypeNameHandling.Auto
            };
            var sceneData = JsonConvert.DeserializeObject<SceneData>(dataJson, settings);

            if (sceneData == null)
            {
                throw new InvalidDataException($"Failed to deserialize data to {typeof(SceneData)}: {dataJson}");
            }

            if (sceneData.Version == SceneData.ActualVersion)
            {
                return sceneData;
            }
            
            // migration from 1.3 to 2.0
            if (sceneData.Version.Major < 2)
            {
                SetColorToBraceSquareDetails(sceneData.SingleDetails);
                foreach (var detailsGroup in sceneData.ConnectedGroups)
                {
                    SetColorToBraceSquareDetails(detailsGroup.Details);
                }
            }

            if (sceneData.Version < SceneData.ActualVersion)
            {
                Debug.Log($"Loaded data migrated from version {sceneData.Version} to {SceneData.ActualVersion}");
                sceneData.Version = SceneData.ActualVersion;
            }
            else
            {
                throw new NotSupportedException($"File data version {sceneData.Version} is above than app supported version {SceneData.ActualVersion}");
            }

            return sceneData;
        }

        private static void SetColorToBraceSquareDetails(IEnumerable<DetailData> details)
        {
            foreach (var detailData in details)
            {
                if (detailData.Type.StartsWith("BraceSquare"))
                {
                    detailData.Color = "Gray";
                }
            }
        }
    }
}